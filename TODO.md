# Things to do

## Implementation

 * Implement `safe_substitute()` from original `string.Template`
 * Include unit tests
   * smart_template
   * hooks
 * Add examples that show the usage of the code
 * Add a module that helps loading pre-defined hooks directly into template objects, using the 
   correct list of argument types
 * Modification of mapping:
   * Allow addition/removal of mapping dictionaries to the object instead of only in the substitute method
   * Allow the template file to be passed in substitute, not (only) at object creation. However, preserve compatibility to `string.Template`
 * Add a function that allows printing of all currently defined hooks and template variables to a file/console (usage of `hook_func.__doc__`)
 * Add helper function to convert a string to boolean, allowing `bool('False')` to return `False`
 
## Documentation
 
 * ~~Finish documentation (Docstrings)~~ *(2019-06-23)*
 * ~~Generate documentation (sphinx)~~ *(2019-06-24)*
 * Expand on the sphinx documentation
   * General descriptions
   * Link to the gitlab source
 
## General

 * Create working pip package
 * Integrate into CI/CD pipeline (gitlab) 
   * ~~Generation of documentation~~ *(2019-06-24)*
   * Running unit tests

