# Changelog

## 0.1.1 (2019-06-24)

### Features
 
 * Added hook `trim()` in the module `hooks.code`, which allows to trim whitespace on one or both sides of the template variable while being aware of multi-line strings  

### Bugfixes

 * Enforced conversion of first argument to a hook to string (`str(value)`)   

## 0.1.0 (2019-06-23)

Initial published version