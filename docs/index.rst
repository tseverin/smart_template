
The smart_template package
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   autodoc/SmartTemplate
   autodoc/hooks
   autodoc/exceptions

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
