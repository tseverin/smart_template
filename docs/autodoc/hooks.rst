Hooks
=======

Formatting
----------
.. automodule:: smart_template.hooks.formatting
    :members:

Code
----------

.. automodule:: smart_template.hooks.code
    :members:

Editing
----------
.. automodule:: smart_template.hooks.edit
    :members:
