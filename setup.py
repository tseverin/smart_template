

__version__ = '0.1.1'

# import general packages
import os

# Well.. setup stuff. Obviously.
from setuptools import setup, find_packages

# Configuration
package_name = 'smart_template'


# helper function to include long texts
def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        return file.read()


setup(
    name=package_name,
    version=__version__,
    description='A smart template replacement engine',
    long_description=read('README.md'),
    author='Timm Severin',
    author_email='timm.severin@tum.de',
    url='https://gitlab.com/tseverin/smart_template',
    license='MIT',
    packages=find_packages(),
    install_requires=[],
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python'
        'Programming Language :: Python :: 3.6'
        'Programming Language :: Python :: 3.7'
    ],
)
